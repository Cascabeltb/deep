package com.mygdx.game.managers;

import java.nio.file.Watchable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.MyGdxGame;
import com.mygdx.game.characters.Enemy;
import com.mygdx.game.characters.Enemy1;
import com.mygdx.game.characters.Platform;
import com.mygdx.game.characters.Player;

public class SpriteManager {
	public MyGdxGame game;
	private Batch batch;
	public OrthographicCamera camera;
	
	ShapeRenderer shapeRenderer;
	public Player player;
	public Array<Enemy> enemys;
	public Array<Platform> platforms;
	
	public Music music;
	
	public SpriteManager(MyGdxGame game){
		this.game=game;
		batch=new SpriteBatch();
		shapeRenderer=new ShapeRenderer();
		camera=new OrthographicCamera();
		camera.setToOrtho(false, 1024,600); //Modificar por par�metros
		//camera.setToOrtho(false, 1024*4,600*4);
		enemys=new Array<Enemy>();
		platforms=new Array<Platform>();
		
		loadCurrentLevel();
	}

	public void loadCurrentLevel(){
		player=new Player(this);
		player.position.set(10,100);
		Enemy1 enem=new Enemy1(this);
		enem.position.x=500;
		enem.position.y=300;
		enemys.add(enem);
		Platform platform=new Platform(this,0,25,500,10);
		platforms.add(platform);
		platform=new Platform(this,400,50,25,25);
		platforms.add(platform);
		platform=new Platform(this,425,50,25,25);
		platforms.add(platform);
		platform=new Platform(this,450,50,25,25);
		platforms.add(platform);
		if(ConfigurationManager.isSoundEnabled()) {
		music=ResourceManager.getMusic("music/Gypsy Cabaret.mp3");
		music.setLooping(true);
		music.play();
		}
		
		
	}
	
	public void update(float dt){
		handleInput();
		player.update(dt);
		for(Enemy enemy : enemys){
			enemy.update(dt);
		}
		camera.position.x=player.position.x;
		camera.position.y=player.position.y;
		camera.update();
		
		
	}
	
	public void draw(){
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		player.render(batch);
		for(Enemy enem : enemys){
			enem.render(batch);
		}
		for(Platform p: platforms){
			p.render(batch);
		}
		//
		batch.end();
		
	}
	
	private void handleInput(){
		
		if(Gdx.input.isKeyJustPressed(Keys.S)){
			ConfigurationManager.EnableSound(!ConfigurationManager.isSoundEnabled());
			if(ConfigurationManager.isSoundEnabled()) {
				System.out.println("El sonido est� activado");
			}else {System.out.println("El NO sonido est� activado");}
			
		}
		if(Gdx.input.isKeyPressed(Keys.SPACE)){
			player.atack();
			
		}
		if(Gdx.input.isKeyPressed(Keys.Z)){
			player.shoot();
		}
		
		if(Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)){
			
			if(player.canJump){
				
				player.jump();
			}
		}
		if(Gdx.input.isKeyPressed(Keys.LEFT) && player.velocity.x>-player.WALKING_SPEED){
			player.isRunning=true;
			player.velocity.x-=Player.WALKING_SPEED/20;
			player.state=Player.State.RUN;
		}
		else if(Gdx.input.isKeyPressed(Keys.RIGHT) && player.velocity.x<player.WALKING_SPEED){
			player.isRunning=true;
			player.velocity.x+= Player.WALKING_SPEED/20;
			player.state=Player.State.RUN;
			
		}
		else{
			
			player.isRunning=false;
			if(player.velocity.x<4 && player.velocity.x>-4){
				player.velocity.x=0;
			player.state=Player.State.IDLE;}
			else if(player.velocity.x>0){
			player.velocity.x-=player.WALKING_SPEED/20;}
			else if (player.velocity.x<0){
				player.velocity.x+=player.WALKING_SPEED/20;
			}
			
			
		}
		
		
	}
}
