package com.mygdx.game.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class ConfigurationManager {

		 
		  private static Preferences prefs = Gdx.app.getPreferences("PreferenciasJuego");;
		 
		  /**
		   * Comprueba si el sonido est� o no activado durante el juego
		   * @return
		   */
		  
		  public static boolean isSoundEnabled() {
		 
		    return prefs.getBoolean("sound",false);
		  }
		  /**
		   * Activa o desactiva el sonido
		   * @return
		   */
		  public static void EnableSound(boolean enable) {
			  prefs.putBoolean("sound", enable);
		        prefs.flush();
			  
		  }
		  
}
