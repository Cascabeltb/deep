package com.mygdx.game.managers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class ResourceManager {
	
	public static AssetManager assets=new AssetManager();
	
	public static void loadAllResources(){
		assets.load("music/Gypsy Cabaret.mp3",Music.class);
		assets.load("sound/rusty_metal_squeak_02.mp3",Sound.class);
		assets.load("characters/DeepOne.pack",TextureAtlas.class);
		assets.load("characters/Bya.pack",TextureAtlas.class);
	//a�adir m�s elementos
	
	}
	
	public static boolean update(){
		return assets.update();
	}
	
	public static TextureAtlas getAtlas(String path){
		return assets.get(path, TextureAtlas.class);
		
	}
	
	public static Music getMusic(String path){
		return assets.get(path, Music.class);
	}
	
	public static Sound getSound(String path)
	{
		return assets.get(path, Sound.class);
	}

	public static void dispose(){
		assets.dispose();
	}
}
