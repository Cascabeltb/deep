package com.mygdx.game.util;

import com.badlogic.gdx.math.Rectangle;

public class Collider {
	
	public Rectangle rect;
	private boolean active;
	
	public Collider(float x, float y, float width, float height){
		this.rect=new Rectangle(x,y,width,height);
		active=false;
	}
	
	public void setActive(boolean act){
		this.active=act;
	}
	
	public boolean isActive(){
		return active;
	}

}
