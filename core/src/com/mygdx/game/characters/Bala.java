package com.mygdx.game.characters;

import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.managers.SpriteManager;

public class Bala extends Character {
	private boolean active;
	private float shootTime;
	private float shootDuration=3;
	public Bala(SpriteManager spriteManager) {
		super(spriteManager);
		// TODO Auto-generated constructor stub
		WIDTH=30;
		HEIGHT=15;
		
		rect=new Rectangle(0,0,WIDTH,HEIGHT);
		active=false;
		
	}

	@Override
	public void update(float dt) {
		// TODO Auto-generated method stub
		if(active && shootTime>0){
		this.position.add(this.velocity.x*dt,this.velocity.y*dt);
		this.rect.setPosition(this.position);
		shootTime-=dt;
		checkCollisions(spriteManager);
		
		
		}else if (shootTime<=0){active=false;}
		
	}

	@Override
	public void checkCollisions(SpriteManager spriteManager) {
		// TODO Auto-generated method stub
		if(active){
			for (Enemy e: spriteManager.enemys){
				if(this.rect.overlaps(e.rect)){
					e.dead=true;
				}
			}
		}
		
	}
	
	public void setActive(){
		this.active=true;
		shootTime=shootDuration;
	}
	public boolean isActive(){
		return this.active;
	}

}
