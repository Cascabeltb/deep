package com.mygdx.game.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.managers.SpriteManager;
import com.mygdx.game.util.Collider;

public class Player extends Character{
	
	public boolean isJumping;
	public boolean canJump;
	public boolean isRunning;
	public State state;
	
	private TextureRegion walk;
	private TextureRegion run;
	private TextureRegion idle;
	private TextureRegion currentFrame;//<--------------

	private ShapeRenderer shapeRenderer;
	private Animation<TextureRegion> walkAnimation;
	private Animation<TextureRegion> runAnimation;
	private Animation<TextureRegion> idleAnimation;
	
	private Collider cajaAtaque;
	private int offsetAtaqueX=-15;
	private int offsetAtaqueY=30;
	private int anchuraAtaque=100;
	private int alturaAtaque=90;
	private float tiempoAtaque=1.5f;
	private float duracionAtaque;
	private boolean derecha=false;
	
	private Array<Bala> balas;
	private int numBalas=600;
	private float delay=0.01f;
	private float atackDuration;
	private int bullet;
	
	
	public enum State{
		IDLE, WALK, RUN
		
	}
	
	
	public static float WALKING_SPEED=70.0f;
	public static float JUMPING_SPEED=250.0f;
	public static float GRAVITY = 9f;
	
	
	
	public Player(SpriteManager spriteManager){
		super(spriteManager);
		position=new Vector2();
		velocity=new Vector2();
		state=State.IDLE;
		
		TextureAtlas atlas=ResourceManager.assets.get("characters/DeepOne.pack",TextureAtlas.class);
		shapeRenderer=new ShapeRenderer();
		idle=atlas.findRegion("walk",2);
		
		WIDTH=idle.getRegionWidth()/3;
		HEIGHT=idle.getRegionHeight()/3;
		
		walkAnimation=new Animation<TextureRegion>(0.15f,atlas.findRegions("walk"));
		runAnimation=new Animation<TextureRegion>(0.15f,atlas.findRegions("run"));
		idleAnimation=new Animation<TextureRegion>(0.15f,atlas.findRegions("stand"));
		
		this.rect=new Rectangle(this.position.x,this.position.y,this.WIDTH, this.HEIGHT);
		this.cajaAtaque=new Collider(this.position.x,this.position.y,this.anchuraAtaque,this.alturaAtaque);
		this.duracionAtaque=0;
		
		this.balas=new Array<Bala>();
		Bala bala;
		for(int i=0; i<numBalas-1; i++){
			bala=new Bala(spriteManager);
			balas.add(bala);
			
		}
		bullet=0;
	}
	
	public void render(Batch spriteBatch){
		stateTime+=Gdx.graphics.getDeltaTime();
		
		switch(state){
		case IDLE: 
			currentFrame=idleAnimation.getKeyFrame(stateTime,true);
			break;
		case RUN: 
			currentFrame=runAnimation.getKeyFrame(stateTime,true);
			break;
		case WALK: 
			currentFrame=walkAnimation.getKeyFrame(stateTime,true);
			break;
		
		}
		if(this.velocity.x>0){
			
			derecha=true;
		}
		else if(this.velocity.x<0){
		derecha=false;
		
		}
		
		if(derecha){
			spriteBatch.draw(currentFrame, position.x+WIDTH,position.y,-WIDTH,HEIGHT);
		}else{spriteBatch.draw(currentFrame, position.x,position.y,WIDTH,HEIGHT);
		}
		spriteBatch.end();
		shapeRenderer.setProjectionMatrix(spriteBatch.getProjectionMatrix());
		
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(Color.GREEN);
		shapeRenderer.rect(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
		if(this.cajaAtaque.isActive()){
			shapeRenderer.setColor(Color.RED);
		}else{shapeRenderer.setColor(Color.YELLOW);}
		shapeRenderer.rect(this.cajaAtaque.rect.x, this.cajaAtaque.rect.y, this.cajaAtaque.rect.width, this.cajaAtaque.rect.height);
		for(Bala b: balas){
			if(b.isActive()){
				shapeRenderer.rect(b.rect.x, b.rect.y, b.rect.width,b.rect.height);
			}
		}
		shapeRenderer.end();
		spriteBatch.begin();
		
	}
	
	public void update(float dt){
		if(duracionAtaque>0){
			duracionAtaque-=dt;
		}else{
			cajaAtaque.setActive(false);
		}
		this.rect.setPosition(this.position);
		this.cajaAtaque.rect.setPosition(this.position.x+offsetAtaqueX,this.position.y+offsetAtaqueY);
		
		if(this.isJumping){
			velocity.y=JUMPING_SPEED;
			this.isJumping=false;
			
		}
		velocity.y-=GRAVITY;
		
		if(velocity.y<-JUMPING_SPEED)
			velocity.y=-JUMPING_SPEED;
		
		this.checkCollisions(spriteManager);
		this.position.add(this.velocity.x*dt,this.velocity.y*dt);
		for(Bala b: balas){
			if(b.isActive()){
				b.update(dt);
			}
		}
		if(atackDuration>0){
			atackDuration-=dt;
		}
	}
	
	public void jump(){
		this.isJumping=true;
		this.canJump=false;
		
	}

	@Override
	public void checkCollisions(SpriteManager spriteManager) {
		// TODO Auto-generated method stub
		for(Platform p: spriteManager.platforms){
			if(this.rect.overlaps(p.rect) && this.velocity.y<0){
				this.velocity.set(new Vector2(this.velocity.x,0));
				this.canJump=true;
			}
			
		}
		for(Enemy e: spriteManager.enemys){
			if(cajaAtaque.isActive()){
				if(cajaAtaque.rect.overlaps(e.rect)){
					e.damage(50);
				}
			}
		}
		
		
	}
	
	public void damage(float force, Enemy enemy){
		if(enemy.position.x>this.position.x){
			this.velocity.x-=force;
		}
		else if(enemy.position.x<this.position.x){
			this.velocity.x+=force;
		}
		this.velocity.y+=force;
		
		
	}
	
	public void atack(){
		this.cajaAtaque.setActive(true);
		duracionAtaque=tiempoAtaque;
		
	}
	
	public void shoot(){
		if(atackDuration<=0){
			atackDuration=delay;
			balas.get(bullet).position.x=this.position.x+50;
			balas.get(bullet).position.y=this.position.y+70;
			int signo;
			if(derecha){
				 signo=1;
			
			}else{ signo=-1;}
			balas.get(bullet).velocity.set(150*signo,0);
			balas.get(bullet).setActive();
			bullet=(bullet+1)%(numBalas-1);
		}
		
	}
	
	
	

}
