package com.mygdx.game.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.managers.SpriteManager;
import com.mygdx.game.util.maths;

public class Enemy1 extends Enemy{
	public State state;
	private Animation<TextureRegion> walkAnimation;
	private Animation<TextureRegion> idleAnimation;
	private Animation<TextureRegion> shoutAnimation;
	private Sound grito;
	private float moveTime=0;
	
	public enum State{
		IDLE, WALK
	}

	public Enemy1(SpriteManager spriteManager) {
		super(spriteManager);
		// TODO Auto-generated constructor stub
		WALKING_SPEED=60;
		position=new Vector2();
		velocity=new Vector2(0,0);
		
		state=State.IDLE;
		
		grito=ResourceManager.getSound("sound/rusty_metal_squeak_02.mp3");
		TextureAtlas atlas=ResourceManager.assets.get("characters/Bya.pack", TextureAtlas.class);
		currentFrame = atlas.findRegion("ByaFloat",1);
		this.WIDTH=currentFrame.getRegionWidth()/3;
		this.HEIGHT=currentFrame.getRegionHeight()/3;
		walkAnimation=new Animation<TextureRegion>(0.05f,atlas.findRegions("ByaFloat"));
		idleAnimation=new Animation<TextureRegion>(0.05f,atlas.findRegions("ByaFloat"));
		shoutAnimation=new Animation<TextureRegion>(0.05f,atlas.findRegions("shout"));
		this.rect=new Rectangle(this.position.x,this.position.y, this.WIDTH, this.HEIGHT);
		
	}

	@Override
	public void update(float dt) {
		if (dead==false){
		this.rect.setPosition(this.position);
		// TODO Auto-generated method stub
		if(state==State.WALK) {
		if(spriteManager.player.position.x<this.position.x && this.velocity.x>-WALKING_SPEED){
			this.velocity.x=this.velocity.x-WALKING_SPEED/50;
		}
		else if(spriteManager.player.position.x>this.position.x && this.velocity.x<WALKING_SPEED){
			this.velocity.x+=WALKING_SPEED/50;
			
		}
		
		if(spriteManager.player.position.y<this.position.y && this.velocity.y>-WALKING_SPEED){
			this.velocity.y-=WALKING_SPEED/50;
		}
		else if(spriteManager.player.position.y>this.position.y && this.velocity.y<WALKING_SPEED){
			this.velocity.y+=WALKING_SPEED/50;
			
		}
		}
		else { if(state==State.IDLE) {
			if (moveTime<=0) {
				moveTime=7;
				this.velocity.x+=MathUtils.randomSign()*WALKING_SPEED/25;
				this.velocity.y+=MathUtils.randomSign()*WALKING_SPEED/25;
				if(this.velocity.x<-WALKING_SPEED) {
					this.velocity.x=-WALKING_SPEED;
				} else if(this.velocity.x>WALKING_SPEED) {
					this.velocity.x=WALKING_SPEED;
				}
				if(this.velocity.y<-WALKING_SPEED) {
					this.velocity.y=-WALKING_SPEED;
				} else if(this.velocity.y>WALKING_SPEED) {
					this.velocity.y=WALKING_SPEED;
				}
				
				System.out.println(maths.distance(this.position, spriteManager.player.position));
			}
			if(maths.distance(this.position, spriteManager.player.position)<250) {
				grito.play();
				state=State.WALK;
				
			}
			moveTime-=dt;
			
			
		}
			
			
		}
		this.position.add(this.velocity.x*dt, this.velocity.y*dt);
		this.checkCollisions(spriteManager);}
	}
	
	public void render(Batch spriteBatch){
		if(dead==false){stateTime+=Gdx.graphics.getDeltaTime();
		switch(state){
		case IDLE:
			currentFrame=idleAnimation.getKeyFrame(stateTime,true);
			break;
		case WALK:
			currentFrame=walkAnimation.getKeyFrame(stateTime,true);
		
		}
		if(this.velocity.x>0){
			spriteBatch.draw(currentFrame, position.x+this.WIDTH, position.y, -this.WIDTH, this.HEIGHT);
		}
		else{spriteBatch.draw(currentFrame, position.x, position.y, this.WIDTH, this.HEIGHT);
		
			
			
		}}
		
	}

	@Override
	public void checkCollisions(SpriteManager spriteManager) {
		// TODO Auto-generated method stub
		if(this.rect.overlaps(spriteManager.player.rect) && dead==false){
			spriteManager.player.damage(75, this);
			
		}
	}

	@Override
	public void damage(float damage) {
		// TODO Auto-generated method stub
		if(spriteManager.player.position.x>this.position.x){
			this.velocity.x-=damage;
		}
		else if(spriteManager.player.position.x<this.position.x){
			this.velocity.x+=damage;
		}
		this.velocity.y+=damage;
		
	}
	

}
