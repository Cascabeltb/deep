package com.mygdx.game.characters;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.managers.SpriteManager;

public class Platform {
	public Vector2 position;
	public Vector2 velocity;
	SpriteManager spriteManager;
	public Rectangle rect;
	TextureRegion currentFrame;
	int anchura;
	int altura;
	
	
	public Platform(SpriteManager spriteManager, int x, int y, int anchura, int altura){
		position=new Vector2(x,y);
		velocity=new Vector2();
		
		this.spriteManager=spriteManager;
		TextureAtlas atlas=ResourceManager.assets.get("characters/DeepOne.pack",TextureAtlas.class);
		if(MathUtils.randomBoolean()==false){
		currentFrame=atlas.findRegion("bloque1");
		}
		else{
			currentFrame=atlas.findRegion("bloque2");	
			
		}
		
		rect=new Rectangle(x,y,anchura, altura);
		this.anchura=anchura;
		this.altura=altura;
	}
	
	public void render(Batch spriteBatch){
		
		spriteBatch.draw(currentFrame,this.position.x,this.position.y,this.anchura, this.altura);
	}

}
