package com.mygdx.game.characters;

import com.mygdx.game.managers.SpriteManager;

public  abstract class Enemy extends Character{
	public float WALKING_SPEED;
	/*Atributos: fuerza, vida, da�o...*/
	
	public Enemy(SpriteManager spriteManager){
		
		super(spriteManager);
	}
	
	public abstract void damage(float damage);


}
